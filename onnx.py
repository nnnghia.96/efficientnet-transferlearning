import models
import torch
import onnxruntime
import numpy as np
from repvgg import get_RepVGG_func_by_name
import cv2
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

repvgg_build_func = get_RepVGG_func_by_name('RepVGG-A0')
model = repvgg_build_func(deploy=False)

checkpoint = torch.load('./outputs/model.pth')
# load model weights state_dict
model.load_state_dict(checkpoint['model_state_dict'])
model.eval()

dummy_input = torch.randn(1, 3, 64, 64) #input size
input_names = [ "actual_input" ]
output_names = [ "output" ]

torch.onnx.export(model, 
                  dummy_input,
                  "repvgg.onnx", # name output file
                  verbose=False,
                  input_names=input_names,
                  output_names=output_names,
                  export_params=True,
                  )